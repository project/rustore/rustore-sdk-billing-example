package ru.rustore.sdk.billingexample.start

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import ru.rustore.sdk.billingclient.RuStoreBillingClient
import ru.rustore.sdk.billingclient.utils.pub.checkPurchasesAvailability
import ru.rustore.sdk.billingexample.start.model.StartPurchasesEvent
import ru.rustore.sdk.billingexample.start.model.StartPurchasesState

class StartPurchasesViewModel : ViewModel() {

    private val _state = MutableStateFlow(StartPurchasesState())
    val state = _state.asStateFlow()

    private val _event = MutableSharedFlow<StartPurchasesEvent>(
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )
    val event = _event.asSharedFlow()

    fun checkPurchasesAvailability() {
        _state.update { it.copy(isLoading = true) }

        RuStoreBillingClient.checkPurchasesAvailability()
            .addOnSuccessListener { result ->
                _state.update { it.copy(isLoading = false) }
                _event.tryEmit(StartPurchasesEvent.PurchasesAvailability(result))
            }
            .addOnFailureListener { throwable ->
                _state.update { it.copy(isLoading = false) }
                _event.tryEmit(StartPurchasesEvent.Error(throwable))
            }
    }
}